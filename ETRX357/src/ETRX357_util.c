#include "ETRX357_util.h"

void ETRX357_Util_uint8To2Char(uint8_t number, char* buff)
{
	if(number > 99)
	{
		number = 99;
	}

	if(number < 10)
	{
		buff[0] = '0';
		buff[1] = number + '0';
	}
	else
	{
		buff[0] = (number / 10) + '0';
		buff[1] = (number % 10) + '0';
	}
}

static uint8_t ETRX357_Util_uint8SingleToHex(uint8_t x)
{
	if(x < 10U)
	{
		x += '0';
	}
	else
	{
		x = (x - 10U) + 'A';
	}

	return x;
}

void ETRX357_Util_Uint8ToHex(uint8_t value, char *buff)
{
	buff[0] = ETRX357_Util_uint8SingleToHex((value >> 4) & 0x0f);
	buff[1] = ETRX357_Util_uint8SingleToHex(value & 0x0f);
}

uint8_t ETRX357_Util_HexToUint8t(char x, char y)
{
	uint8_t temp = 0;

	if(x < 'A')
	{
		temp = (x - '0') << 4;
	}
	else
	{
		temp = (x - 'A' + 10) << 4;
	}

	if(y < 'A')
	{
		temp |= (y - '0');
	}
	else
	{
		temp |= (y - 'A' + 10);
	}

	return temp;
}
