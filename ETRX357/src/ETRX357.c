#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include <ETRX357.h>
#include <ETRX357_registers.h>

#include "ETRX357_util.h"

#include <board.h>

/**********************************************************************************************************************/
/**                                            BASIC DATA TYPEDEFS                                                   **/
/**********************************************************************************************************************/

typedef void (*ETRX357_uart_response_function_t)(char **paramsArray, uint8_t paramsNumber);

typedef void (*ETRX357_UartSpecialResponceFunction_t)(const char *responseBuff);

/**
 * Typedef for callback in case of error.
 */
typedef void (*ETRX357_error_callback_t)(void);

/**
 * Info about response from ETRX357 module - for easier decoding
 */
typedef struct
{
	const char* rsp;							//! response from module
	uint8_t length;								//! length of response
	bool    full_parse;							//! if true - this is internal command, if false - comment content (like bcast reponse) is parsed by user.
	ETRX357_uart_response_function_t function;	//! function to call after parsing
}ETRX357_uartResponse_t;

/**
 * Enum with commands with different parse method - for example response for ATI command takes 3 lines.
 */
typedef enum
{
	ETRX_specialResponse_none = 0,
	ETRX_specialResponse_ATI,
	ETRX_specialResponse_ATS,
}ETRX_specialResponse_e;

/**
 * Enum type for multiline response - when every line is in the same format.
 */
typedef enum
{
	ETRX357_multilineResponse_none = 0,
	ETRX357_multilineResponse_PANSCAN,
}ETRX357_multilineResponse_e;

typedef struct
{
	char ID[3];
	ETRX357_error_callback_t callback;
}ETRX357_error_callbacks_s;

/**********************************************************************************************************************/
/**                                                 MODULE VARIABLES                                                 **/
/**********************************************************************************************************************/

/**
 * Actual module config from init.
 */
static ETRX357_InitParameters_s *ETRX357_struct;

/**
 * MAC received from module.
 */
static MAC ETRX357_mac;

/**
 * Actual network params.
 */
static ZIGBEE_NETWORK_PARAMS ZB_network_params;

/**
 * Actual received special response type.
 */
static ETRX_specialResponse_e ETRX357_specialResponse = ETRX_specialResponse_none;

/**
 * Counter for received lines in case of multiline response.
 */
static uint8_t ETRX357_specialResponseLineCounter;

/**
 * Actual enabled multilineResponse type
 */
static ETRX357_multilineResponse_e ETRX357_multilineResponse = ETRX357_multilineResponse_none;

/**
 * Array with error - usefull during development/debug phase
 * Data from module documentation.
 * The best - "Error 00" - this means "success"
 */
#if ETRX357_DEBUG_LVL > 2
static const char *errors_messages_array[] =
{
#include "ETRX357_errors_messages.h"
};
#endif

/**********************************************************************************************************************/
/**                                                 MODULE FUNCTIONS                                                 **/
/**********************************************************************************************************************/

/**
 * Error A1 - too many broadcasts in network!
 */
static void ERTX357_ErrorHandler_A1(void)
{
	ETRX357_DEBUG("BROADCAST LIMIT ERROR\r\n");

	if(ETRX357_struct->cb_broadcast_limit_error)
	{
		ETRX357_struct->cb_broadcast_limit_error();
	}
}

/**
 * Error 94 or AD - can't join to selected network.
 */
static void ERTX357_ErrorHandler_94_AD(void)
{
	ETRX357_DEBUG("CAN'T CONNECT TO NETWORK\r\n");

	if(ETRX357_struct->cb_network_join_fail)
	{
		ETRX357_struct->cb_network_join_fail();
	}
}

static const ETRX357_error_callbacks_s ETRX357_errors[] =
{
 	 { "A1", ERTX357_ErrorHandler_A1    },
 	 { "94", ERTX357_ErrorHandler_94_AD },
 	 { "AD", ERTX357_ErrorHandler_94_AD },
};

static const uint8_t ERRORS_CALLBACKS_ARRAY_SIZE = sizeof(ETRX357_errors)/sizeof(ETRX357_errors[0]);

/**
 * Callback after receiving unknown prompt from module.
 * @param data received data
 */
static void ETRX357_PromptDefaultCallback(char *data)
{
	ETRX357_DEBUG("Default promt handler\r\n");
	ETRX357_DEBUG("Received data: %s\r\n", data);
}

void ETRX357_Init(ETRX357_InitParameters_s *config)
{
	ETRX357_struct = config;
}

/**
 * Parse response from device with error message.
 * @param params_array array with parameters
 * @param params_cnt   how many parameters in array
 */
static void ETRX357_PromptErrorCallback(char **params_array, uint8_t params_cnt)
{
	(void)params_cnt;

	const uint8_t ERROR_ID_LEN = 2U;

	ETRX357_DEBUG("Error prompt handler. Error number: %s", params_array[0]);

#if ETRX357_DEBUG_LVL > 2
	for(uint8_t i = 0U; i < sizeof(errors_messages_array)/sizeof(errors_messages_array[0]); i++)
	{

		if(strncmp(errors_messages_array[i], params_array[0], ERROR_ID_LEN) == 0)
		{
			ETRX357_DEBUG("Error type: %s", errors_messages_array[i]);

			break;
		}
	}
#endif

	for(uint8_t i = 0U; i < ERRORS_CALLBACKS_ARRAY_SIZE; i++)
	{
		if(strncmp(ETRX357_errors[i].ID, params_array[0], ERROR_ID_LEN) == 0)
		{
			if(ETRX357_errors[i].callback)
			{
				ETRX357_errors[i].callback();
			}

			break;
		}
	}
}

/**
 * Parse received broadcast.
 * Example message -> 000D6F000E9DF6B5,04=TEST.
 * @param params_array
 * @param params_cnt
 */
static void ETRX357_PromptParse_Broadcast(char **params_array, uint8_t params_cnt)
{
	(void)params_cnt;

	ETRX357_DEBUG("BCAST received");
	ETRX357_DEBUG("Content: %s", params_array[0]);

	MAC* senderMAC = (MAC *)params_array[0];

	params_array[0][sizeof(MAC) - 1U] = '\0';

	const uint8_t MESSSAGE_START_POSITION_IN_BCAST = 20U;
	char *receivedMessage = params_array[0] + MESSSAGE_START_POSITION_IN_BCAST;

	if(ETRX357_struct->cb_broadcast)
	{
		ETRX357_struct->cb_broadcast(senderMAC, receivedMessage);
	}
}

/**
 * Parse received unicast.
 * Example message -> 000D6F000E9DF6B5,04=TEST.
 * @param params_array
 * @param params_cnt
 */
static void ETRX357_PromptParse_Unicast(char **params_array, uint8_t params_cnt)
{
	(void)params_cnt;

	ETRX357_DEBUG("UCAST received");
	ETRX357_DEBUG("Content: %s", params_array[0]);

	MAC* senderMAC = (MAC *)params_array[0];

	params_array[0][sizeof(MAC) - 1U] = '\0';

	const uint8_t MESSSAGE_START_POSITION_IN_UCAST = 20U;
	char *receivedMessage = params_array[0] + MESSSAGE_START_POSITION_IN_UCAST;

	if(ETRX357_struct->cb_unicast)
	{
		ETRX357_struct->cb_unicast(senderMAC, receivedMessage);
	}
}

/**
 * Parse FFD frame - info about new device in network.
 * @param params_array
 * @param params_cnt
 */
static void ETRX357_PromptParse_FFD(char **params_array, uint8_t params_cnt)
{
	(void)params_cnt;

	ETRX357_DEBUG("New device in network");
	ETRX357_DEBUG("MAC: %s", params_array[0]);
	ETRX357_DEBUG("ID: %s", params_array[1]);

	if(ETRX357_struct->cb_new_device_joined)
	{
		ETRX357_struct->cb_new_device_joined((MAC*)params_array[0]);
	}
}

/**
 * Parse "+N" response.
 * "NoPan" - in case of no active connection
 * "+N=COO,24,08,ED59,F3E79998B4A96ACC" in case of active connection
 * @param params_array
 * @param params_cnt
 */
static void ETRX357_PromptParse_N(char **params_array, uint8_t params_cnt)
{
	if(params_cnt == 1U)
	{
		const char noNetworkMSG[] = "NoPan";

		if(strcmp(params_array[0], noNetworkMSG) == 0)
		{
			ZB_network_params.channel = 0U;
			ETRX357_DEBUG("No network");
		}
	}
	else
	{
		ZB_network_params.channel = atoi(params_array[1]);

		ETRX357_CopyPID(ZB_network_params.network_PID, params_array[3]);
		ETRX357_CopyEPID(ZB_network_params.network_EPID, params_array[4]);

		ETRX357_DEBUG("Channel: %d", ZB_network_params.channel);
		ETRX357_DEBUG("PID: %s",     ZB_network_params.network_PID);
		ETRX357_DEBUG("EPID: %s",    ZB_network_params.network_EPID);
	}

	if(ETRX357_struct->cb_get_network_info)
	{
		ETRX357_struct->cb_get_network_info(&ZB_network_params);
	}
}

/**
 * Parse "PANSCAN" response.
 * example: +PANSCAN:12,EAD3,E20EB771F73747D5,02,01
 * @param params_array
 * @param params_cnt
 */
static void ETRX357_PromptParse_PANSCAN(char **params_array, uint8_t params_cnt)
{
	(void)params_cnt;
	ETRX357_DEBUG("Network data received");
	ETRX357_DEBUG("k:%s", params_array[0]);
	ETRX357_DEBUG("P:%s", params_array[1]);
	ETRX357_DEBUG("EP:%s", params_array[2]);

	ZIGBEE_NETWORK_PARAMS network_params;

	network_params.channel = atoi(params_array[0]);

	ETRX357_CopyPID(network_params.network_PID, params_array[1]);
	ETRX357_CopyEPID(network_params.network_EPID, params_array[2]);

	network_params.ZB_stack_profile = atoi(params_array[3]);
	network_params.ZB_joining_permitted = atoi(params_array[4]);

	ETRX357_DEBUG("Channel: %d", network_params.channel);
	ETRX357_DEBUG("PID: %s", network_params.network_PID);
	ETRX357_DEBUG("EPID: %s", network_params.network_EPID);

	if(ETRX357_struct->cb_network_scan_found)
	{
		ETRX357_struct->cb_network_scan_found(&network_params);
	}
}

/**
 * Parse "JPAN" response.
 * Promt after joining to other network, but also after creating own network.
 * example: +PANSCAN:12,EAD3,E20EB771F73747D5,02,01
 * @param params_array
 * @param params_cnt
 */
static void ETRX357_PromptParse_JPAN(char **params_array, uint8_t params_cnt)
{
	(void)params_cnt;
	ETRX357_DEBUG("Joined to network");

	ETRX357_DEBUG("CH:%s", params_array[0]);
	ETRX357_DEBUG("P:%s", params_array[1]);
	ETRX357_DEBUG("EP:%s", params_array[2]);

	ZB_network_params.channel = atoi(params_array[0]);

	ETRX357_CopyPID(ZB_network_params.network_PID, params_array[1]);
	ETRX357_CopyEPID(ZB_network_params.network_EPID, params_array[2]);

	if(ETRX357_struct->cb_joined_to_network)
	{
		ETRX357_struct->cb_joined_to_network();
	}
}

/**
 * Parse "LeftPAN" response.
 * @param params_array
 * @param params_cnt
 */
static void ETRX357_PromptParse_LeftPAN(char **params_array, uint8_t params_cnt)
{
	ETRX357_DEBUG("Disconnected from network");

	if(ETRX357_struct->cb_leave_network)
	{
		ETRX357_struct->cb_leave_network();
	}
}

/**
 * Variables for parsing ATS response.
 */
static uint8_t ETRX357_ATS_lastRegId = 0U;
static uint8_t ETRX357_ATS_lastValueBuff[15];
static uint8_t ETRX357_ATS_lastValueBuffLength = 0U;

/**
 * Function called after "OK" from ETRX. Decode response.
 * @param params_array for compability with ATParser
 * @param params_cnt   for compability with ATParser
 */
void ETRX357_PromtParse_ok(char** params_array, uint8_t params_cnt)
{
	(void)params_array;
	(void)params_cnt;

	if(ETRX357_specialResponse != ETRX_specialResponse_none)
	{
		switch(ETRX357_specialResponse)
		{
			case ETRX_specialResponse_none:
			{

				break;
			}
			case ETRX_specialResponse_ATI:
			{
				if(ETRX357_struct->cb_get_MAC)
				{
					ETRX357_struct->cb_get_MAC();
				}

				break;
			}
			case ETRX_specialResponse_ATS:
			{
				if(ETRX357_struct->cb_getRegValue)
				{
					ETRX357_struct->cb_getRegValue(ETRX357_ATS_lastRegId, ETRX357_ATS_lastValueBuff, ETRX357_ATS_lastValueBuffLength);
				}

				break;
			}
		}

		ETRX357_specialResponse = ETRX_specialResponse_none;
		ETRX357_specialResponseLineCounter = 0U;
	}
	else if(ETRX357_multilineResponse != ETRX357_multilineResponse_none)
	{
		switch(ETRX357_multilineResponse)
		{
			case ETRX357_multilineResponse_none:
			{

				break;
			}
			case ETRX357_multilineResponse_PANSCAN:
			{
				if(ETRX357_struct->cb_network_scan_completed)
				{
					ETRX357_struct->cb_network_scan_completed();
				}

				break;
			}
		}

		ETRX357_multilineResponse = ETRX357_multilineResponse_none;
	}
}

void ETRX357_GetMAC_Command(void)
{
	ETRX357_specialResponse = ETRX_specialResponse_ATI;
	ETRX357_specialResponseLineCounter = 0U;

	ETRX357_struct->send_rn("ATI");
}

void ETRX357_GetNetworkInfo_Command(void)
{
	ETRX357_struct->send_rn("AT+N");
}

const ZIGBEE_NETWORK_PARAMS* ETRX357_GetNetworkInfoAfterJoin(void)
{
	return &ZB_network_params;
}

/**
 * Parse ATI command response.
 * @param responseBuff - received buff
 */
void ATI_response(const char *responseBuff)
{
	switch(ETRX357_specialResponseLineCounter)
	{
		case 0:
		{
			ETRX357_DEBUG("ATI - module name: %s", responseBuff);
			ETRX357_DEBUG("%s", responseBuff);
			ETRX357_specialResponseLineCounter++;
			break;
		}
		case 1:
		{
			ETRX357_DEBUG("ATI - firmware version: %s", responseBuff);
			ETRX357_specialResponseLineCounter++;
			break;
		}
		case 2:
		{
			ETRX357_DEBUG("ATI - device MAC: %s", responseBuff);
			strncpy(ETRX357_mac, responseBuff, sizeof(ETRX357_mac));
		}
	}
}

/**
 * Parse ATS_response. Received data in hex format.
 * @param buff - received buff
 */
static void ATS_response(const char *buff)
{
	ETRX357_ATS_lastValueBuffLength = 0;

	char c = 0;

	while((c = (*buff)))
	{
		ETRX357_ATS_lastValueBuff[ETRX357_ATS_lastValueBuffLength] = ETRX357_Util_HexToUint8t(*buff, *(buff + 1));
		buff += 2;
		ETRX357_ATS_lastValueBuffLength++;
	}
}

static ETRX357_UartSpecialResponceFunction_t uartSpecialResponsesArray[] =
{
		0,
		ATI_response,
		ATS_response,
};

/**
 * Utils for arrays
 */
#define _ETRX357_PROMPT(x) 		(x),(sizeof(x)-1)
#define _ETRX357_RESPONSE(x) 	(x),(sizeof(x)-1)

/**
 * Array with callbacks for module prompts parse.
 */
const ETRX357_uartResponse_t ETRX357_uart_resp_tab[] =
{
    {_ETRX357_PROMPT("OK")        ,  true, ETRX357_PromtParse_ok},
    {_ETRX357_PROMPT("ERROR:")    ,  true, ETRX357_PromptErrorCallback},
    {_ETRX357_PROMPT("ACK:")      ,  true, NULL},
    {_ETRX357_PROMPT("NACK:")     ,  true, NULL},
    {_ETRX357_PROMPT("POLLED:")   ,  true, NULL},
    {_ETRX357_PROMPT("SR:")       ,  true, NULL},
    {_ETRX357_PROMPT("BCAST:")    , false, ETRX357_PromptParse_Broadcast},
    {_ETRX357_PROMPT("MCAST:")    , false, NULL},
    {_ETRX357_PROMPT("UCAST:")    , false, ETRX357_PromptParse_Unicast},
    {_ETRX357_PROMPT("INTERPAN:") , false, NULL},
    {_ETRX357_PROMPT("RAW:")      ,  true, NULL},
    {_ETRX357_PROMPT("SDATA:")    ,  true, NULL},
    {_ETRX357_PROMPT("FFD:")      ,  true, ETRX357_PromptParse_FFD},
    {_ETRX357_PROMPT("SED:")      ,  true, NULL},
    {_ETRX357_PROMPT("MED:")      ,  true, NULL},
    {_ETRX357_PROMPT("ZED:")      ,  true, NULL},
    {_ETRX357_PROMPT("NEWNODE:")  ,  true, NULL},
    {_ETRX357_PROMPT("LeftPAN")   ,  true, ETRX357_PromptParse_LeftPAN},
    {_ETRX357_PROMPT("LostPAN")   ,  true, NULL},
    {_ETRX357_PROMPT("JPAN:")     ,  true, ETRX357_PromptParse_JPAN},
    {_ETRX357_PROMPT("NODELEFT:") ,  true, NULL},
    {_ETRX357_PROMPT("ADSK:")     ,  true, NULL},
    {_ETRX357_PROMPT("Bind:")     ,  true, NULL},
    {_ETRX357_PROMPT("Unbind:")   ,  true, NULL},
    {_ETRX357_PROMPT("TRACK:")    ,  true, NULL},
    {_ETRX357_PROMPT("TRACK2:")   ,  true, NULL},
    {_ETRX357_PROMPT("AddrResp:") ,  true, NULL},
    {_ETRX357_PROMPT("RX:")       ,  true, NULL},
    {_ETRX357_PROMPT("NM:")       ,  true, NULL},
    {_ETRX357_PROMPT("+N=")       ,  true, ETRX357_PromptParse_N},
    {_ETRX357_PROMPT("+PANSCAN:") ,  true, ETRX357_PromptParse_PANSCAN},
};

#define ETRX_MAX_PARAMETERS (20)

void ETRX357_DecodeReceivedData(char *data)
{
	bool commandRecognized = false;

	if(ETRX357_specialResponse != ETRX_specialResponse_none)
	{
		ETRX357_DEBUG("Special response received\r\n");

		if(strcmp(data, "OK") == 0)
		{
			ETRX357_PromtParse_ok(&data, 1);
		}
		else
		{
			(uartSpecialResponsesArray[ETRX357_specialResponse])(data);
		}

		commandRecognized = true;
	}

	if(ETRX357_multilineResponse != ETRX357_multilineResponse_none)
	{
		ETRX357_DEBUG("Multiline response detected\r\n");

		/* if OK is received during multiline response */
		if(strcmp(data, "OK") == 0)
		{
			ETRX357_PromtParse_ok(&data,1);
			commandRecognized = true;
		}
	}

	/* TODO - split later into subfunctions/integrate with ATParser */
	if(commandRecognized == false)
	{
		for(uint8_t i = 0U; i < sizeof(ETRX357_uart_resp_tab)/sizeof(ETRX357_uart_resp_tab[0]); i++)
		{
			if(strncmp(data, ETRX357_uart_resp_tab[i].rsp, ETRX357_uart_resp_tab[i].length) == 0)
			{
				ETRX357_DEBUG("Response: %s", ETRX357_uart_resp_tab[i].rsp);

				if(ETRX357_uart_resp_tab[i].function)
				{
				    char* params[ETRX_MAX_PARAMETERS];

				    uint8_t params_cnt = 0;

					if(ETRX357_uart_resp_tab[i].full_parse)
					{
						data = data + ETRX357_uart_resp_tab[i].length;

						if(data[0] != 0)
						{
							char *wsk_temp;
							params[0] = strtok_r(data, ",", &wsk_temp);

							if(params[0] != 0)
							{
								params_cnt++;

								for(int i = 1; i < ETRX_MAX_PARAMETERS; i++)
								{
									params[i] =  strtok_r(NULL, ",",&wsk_temp);

									if(params[i] == NULL)
									{
										break;
									}

									params_cnt++;
								}
							}
						}

						ETRX357_uart_resp_tab[i].function(params, params_cnt);
					}
					else
					{
						params[0] = data + ETRX357_uart_resp_tab[i].length;

						params_cnt = 1U;

						ETRX357_uart_resp_tab[i].function(params, params_cnt);
					}

					commandRecognized = true;
				}
				else
				{
					ETRX357_PromptDefaultCallback(data);
				}

				break;
			}
		}
	}

	if(commandRecognized == false)
	{
		ETRX357_DEBUG("Command not found: %s", data);
	}
}

const MAC* ETRX357_GetMAC_AfterCommand(void)
{
	return &ETRX357_mac;
}

void ETRX357_Set_EchoDisableBaudrate115200(void)
{
	ETRX357_struct->send_rn("ATS12=0C10");
}

void ETRX357_Network_Create(void)
{
	ETRX357_struct->send_rn("AT+EN");
}

void ETRX357_Network_Leave(void)
{
	ETRX357_struct->send_rn("AT+DASSL");
}

void ETRX357_Network_JoinRandomNetwork(void)
{
	ETRX357_struct->send_rn("AT+JN");
}

void ETRX357_Network_Join(const ZIGBEE_NETWORK_PARAMS *network)
{
	ETRX357_DEBUG("Join to network: %s", network->network_PID);

	char channelString[3] = { 0 };

	ETRX357_Util_uint8To2Char(network->channel, channelString);

	char joinCommand[40] = "AT+JPAN:";

	strcat(joinCommand, channelString);
	strcat(joinCommand, ",");
	/* both PID or EPID are ok for this command - bud PID is smaller :) */
	strcat(joinCommand, network->network_PID);

	ETRX357_struct->send_rn(joinCommand);
}

void ETRX357_SendBroadcast(uint8_t hops, const char *message)
{
	char number[3] = "00";
	ETRX357_Util_uint8To2Char(hops,number);

	char broadcastCommand[120] = "AT+BCAST=";

	char *catPosition = strcat(broadcastCommand, number);

	catPosition = strcat(catPosition, ",");
	catPosition = strcat(catPosition, message);

	ETRX357_struct->send_rn(broadcastCommand);
}

void ETRX357_SendUnicast(const MAC *mac, const char *message)
{
	char unicastCommand[120] = "AT+UCAST=";

	char *macAsStr = (char *)mac;

	char *catPosition = strcat(unicastCommand, macAsStr);

	catPosition = strcat(catPosition, ",");
	catPosition = strcat(catPosition, message);

	ETRX357_struct->send_rn(unicastCommand);
}

void ETRX357_ScanNetworksStart(void)
{
	ETRX357_struct->send_rn("AT+PANSCAN");
	ETRX357_multilineResponse = ETRX357_multilineResponse_PANSCAN;
}

void ETRX357_WriteRegisterValue(uint8_t reg_id, const uint8_t *value, uint8_t length)
{
	char buffRegisterNumberInHex[3] = "00";
	ETRX357_Util_Uint8ToHex(reg_id, buffRegisterNumberInHex);

	char buffValueHexString[20];

	for(uint8_t i = 0U; i < length; i++)
	{
		ETRX357_Util_Uint8ToHex(value[i], buffValueHexString + (i * 2U));
	}

	buffValueHexString[length * 2U] = '\0';

	char atsCommand[30] = "ATS";

	strcat(atsCommand, buffRegisterNumberInHex);
	strcat(atsCommand, "=");
	strcat(atsCommand, buffValueHexString);

	ETRX357_struct->send_rn(atsCommand);
}

void ETRX357_ReadRegisterValue(uint8_t reg_id)
{
	ETRX357_ATS_lastRegId = reg_id;
	ETRX357_specialResponse = ETRX_specialResponse_ATS;
	ETRX357_specialResponseLineCounter = 0;

	char atsCommand[30] = "ATS";

	char regNumberString[3] = "00";
	ETRX357_Util_Uint8ToHex(reg_id, regNumberString);

	strcat(atsCommand, regNumberString);
	strcat(atsCommand, "?");

	ETRX357_struct->send_rn(atsCommand);
}

void ETRX357_AutojoinDisable(void)
{
	ETRX357_struct->send_rn("ATS02=FFFE");
}

void ETRX357_AutojoinEnable(void)
{
	ETRX357_struct->send_rn("ATS02=0000");
}

void ETRX357_Util_CopyNetworkParam(ZIGBEE_NETWORK_PARAMS *dest, const ZIGBEE_NETWORK_PARAMS *source)
{
	memcpy(dest, source, sizeof(ZIGBEE_NETWORK_PARAMS));
}

void ETRX357_PromptDisable(uint16_t promt_disable)
{
    /**
     * Convert received uint16_t data to array of bytes (uint8_t type) for WriteRegisterValue function.
     */
	uint8_t registerValue[2];

	registerValue[0] = promt_disable >> 8;
	registerValue[1] = promt_disable;

	ETRX357_WriteRegisterValue(ETRX_REGISTER_PROMPT_ENABLE_1, registerValue, sizeof(registerValue));
}

void ETRX357_CopyPID(char *to, const char *from)
{
	strncpy(to, from, sizeof(PID));
}

void ETRX357_CopyEPID(char *to, const char *from)
{
	strncpy(to, from, sizeof(EPID));
}
