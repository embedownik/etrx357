/*
 * ETRX357_util.h
 *
 *      Author: Embedownik
 *
 *  Some helper functions for etrx module.
 *  "Custom" functions for data convert - this is some my old "premature optimalization" :/
 */

#ifndef ETRX357_UTIL_H_
#define ETRX357_UTIL_H_

#include <stdint.h>

void ETRX357_Util_uint8To2Char(uint8_t number, char* buff);

void ETRX357_Util_Uint8ToHex(uint8_t value, char *buff);

uint8_t ETRX357_Util_HexToUint8t(char x, char y);

#endif
