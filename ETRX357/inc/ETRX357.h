/*
 * ETRX357.h
 *
 *      Author: Embedownik
 *
 *  Module for communication with ETRX357 - Zigbee network module.
 *
 */

#ifndef ETRX357_H_
#define ETRX357_H_

#include <stdbool.h>
#include <stdint.h>

/**********************************************************************************************************************/
/**                                            BASIC DATA TYPEDEFS                                                   **/
/**********************************************************************************************************************/

/**
 * Type for device MAC (16B).
 * Size increased by 1 to keep as string in format easy to display.
 */
typedef char MAC[17];

/**
 * Type for network PID (PersonalAreaNetwork ID) (4B)
 * Size increased by 1 to keep as string in format easy to display.
 */
typedef char PID[5];

/**
 * Type for network EPID (Extended PersonalAreaNetwork ID) (16B)
 * Size increased by 1 to keep as string in format easy to display.
 */
typedef char EPID[17];

/**
 * Zigbee network parameters.
 * Used in commands like networkScan/joinNetwork.
 */
typedef struct ZIGBEE_NETWORK_PARAMS
{
	uint8_t channel;
	PID 	network_PID;
	EPID	network_EPID;
	uint8_t ZB_stack_profile;
	uint8_t ZB_joining_permitted;
}ZIGBEE_NETWORK_PARAMS;

/**********************************************************************************************************************/
/**                                                CALLBACKS TYPEDEFS                                                **/
/**********************************************************************************************************************/

/**
 * Function to send data to module (with auto rn at the end)
 * @param  data to send
 */
typedef void (*send_rn_function_t)(char *buff);

/**
 * Function to send data to module
 * @param  data to send
 */
typedef void (*send_function_t)(char *buff);

/**
 * Callback called after receiving parameter from module.
 * @param regNumber
 * @param receivedData
 * @param dataLen
 */
typedef void (*ETRX357_cb_GetRegValue_t)(uint8_t regNumber, uint8_t *receivedData, uint8_t dataLen);

/**
 * Callback called after receiving MAC from device.
 */
typedef void (*ETRX357_cb_GetMAC_t)(void);

/**
 * Callback called in case network leave event.
 */
typedef void (*ETRX357_cb_LeaveNetwork_t)(void);

/**
 * Callback called in case network join event.
 */
typedef void (*ETRX357_cb_JoinedToNetwork_t)(void);

/**
 * Callback called in case of "getNetworkInfo" event.
 * @param networkParams
 */
typedef void (*ETRX357_cb_GetNetworkInfo_t)(const ZIGBEE_NETWORK_PARAMS *networkParams);

/**
 * Callback called after receiving broadcast from network.
 * @param senderMAC
 * @param message
 */
typedef void (*ETRX357_cb_BroadcastReceived_t)(const MAC* senderMAC, char *message);

/**
 * Callback called after receiving unicast from network.
 * @param senderMAC
 * @param message
 */
typedef void (*ETRX357_cb_UnicastReceived_t)(const MAC* senderMAC, char *message);

/**
 * Callback called in case of "newNetworkFoundDuringScan" event.
 * @param networkParams new network params
 */
typedef void (*ETRX357_cb_NetworkScanFound_t)(ZIGBEE_NETWORK_PARAMS* networkParams);

/**
 * Callback called after end of "ScanNetwork" procedure.
 * @param networkParams new network params
 */
typedef void (*ETRX357_cb_NetworkScanCompleted_t)(void);

/**
 * Callback called after "NewDeviceInNetwork" event.
 * @param newDeviceMAC
 */
typedef void (*ETRX357_cb_NewDeviceJoined_t)(MAC* newDeviceMAC);

/**
 * Callback called after "tooManyBroadcastInNetwork" event.
 */
typedef void (*ETRX357_cb_BroadcastLimitError_t)(void);

/**
 * Callback called after "NetworkJoinFailed" event.
 */
typedef void (*ETRX357_cb_NetworkJoinFail_t)(void);

/**
 * All parameters/callbacks for module.
 */
typedef struct ETRX357_init_struct
{
	send_function_t send;
	send_rn_function_t send_rn;

	ETRX357_cb_GetRegValue_t cb_getRegValue;

	ETRX357_cb_GetMAC_t cb_get_MAC;

	ETRX357_cb_BroadcastReceived_t cb_broadcast;
	ETRX357_cb_UnicastReceived_t cb_unicast;

	ETRX357_cb_GetNetworkInfo_t cb_get_network_info;

	ETRX357_cb_LeaveNetwork_t cb_leave_network;
	ETRX357_cb_JoinedToNetwork_t cb_joined_to_network;

	ETRX357_cb_NetworkScanFound_t cb_network_scan_found;
	ETRX357_cb_NetworkScanCompleted_t cb_network_scan_completed;

	ETRX357_cb_NewDeviceJoined_t cb_new_device_joined;

	ETRX357_cb_BroadcastLimitError_t cb_broadcast_limit_error;

	ETRX357_cb_NetworkJoinFail_t cb_network_join_fail;
}ETRX357_InitParameters_s;

/**********************************************************************************************************************/
/**                                                    FUNCTIONS                                                     **/
/**********************************************************************************************************************/

/**
 * Init module.
 * @param config module configuration
 */
void ETRX357_Init(ETRX357_InitParameters_s *config);

/**
 * Received data decoder. Insert in UART module receive line callback.
 * @param buff received data
 */
void ETRX357_DecodeReceivedData(char *buff);

/**
 * Set "recommended" settings.
 */
void ETRX357_Set_EchoDisableBaudrate115200(void);

/**
 * Send to module request for MAC.
 * Response in separate callback.
 */
void ETRX357_GetMAC_Command(void);

/**
 * Get pointer to actual MAC
 * @return pointer to device MAC
 */
const MAC* ETRX357_GetMAC_AfterCommand(void);

/**
 * Get actual network info from module.
 * Response in separate callback.
 */
void ETRX357_GetNetworkInfo_Command(void);

/**
 * Function to get network info - but only in "joinedToNetwork" callback - because
 * this is available in this callback directly from library.
 * In other cases - send separate command.
 * @return
 */
const ZIGBEE_NETWORK_PARAMS* ETRX357_GetNetworkInfoAfterJoin(void);

/**
 * Create new ZB network.
 */
void ETRX357_Network_Create(void);

/**
 * Leave current ZB network.
 */
void ETRX357_Network_Leave(void);

/**
 * Join to random unsecured network in range.
 */
void ETRX357_Network_JoinRandomNetwork(void);

/**
 * Join to selected network.
 * @param network network to join
 */
void ETRX357_Network_Join(const ZIGBEE_NETWORK_PARAMS *network);

/**
 * Send broadcast - message to the whole network.
 * @param hops number of hops for selected message (0 means to all)
 * @param message message to send
 */
void ETRX357_SendBroadcast(uint8_t hops, const char *message);

/**
 * Send unicast message to selected device.
 * @param mac     MAC of receiver
 * @param message message to send
 */
void ETRX357_SendUnicast(const MAC *mac, const char *message);

/**
 * Command to start scan network procedure.
 * Results in callbacks from settings struct.
 */
void ETRX357_ScanNetworksStart(void);

/**
 * Write value to selected register.
 * Value as buff - because of registers with different length.
 *
 * For example - to set some 4 byte reg to value 0xabcd values in buff should be like this: { 0xab, 0xcd }
 *
 * @param reg_id register id
 * @param value  value to write
 * @param length length of data to write/length of value array
 */
void ETRX357_WriteRegisterValue(uint8_t reg_id, const uint8_t *value, uint8_t length);

/**
 * Command to read selected register value.
 * Response in separate callback.
 *
 * @param reg_id id of register
 */
void ETRX357_ReadRegisterValue(uint8_t reg_id);

/**
 * Enable autojoin feature.
 */
void ETRX357_AutojoinDisable(void);

/**
 * Disable autojoin feature.
 */
void ETRX357_AutojoinEnable(void);

/**
 * Copy structs with network parameters - just wrapper over memcpy.
 * @param dest   destination
 * @param source source
 */
void ETRX357_Util_CopyNetworkParam(ZIGBEE_NETWORK_PARAMS *dest, const ZIGBEE_NETWORK_PARAMS *source);

/**
 * Disable selected prompts in module.
 * @param promt_disable - from macro "ETRX_PROMPT_DISABLE_xxx"
 */
void ETRX357_PromptDisable(uint16_t promt_disable);

void ETRX357_CopyPID(char *to, const char *from);

void ETRX357_CopyEPID(char *to, const char *from);

#endif
